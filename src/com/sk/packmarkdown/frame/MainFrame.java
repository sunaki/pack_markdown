package com.sk.packmarkdown.frame;

import com.sk.packmarkdown.code.MarkDownUtils;
import com.sk.packmarkdown.code.StringUtils;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.io.File;

public class MainFrame extends JFrame {
    private Integer screenWidth;
    private Integer screenHeight;
    private Integer cWidth = 650;
    private Integer cHeight = 500;
    private Container c;

    /**
     * 获取Windows显示器的宽高分辨率
     */ {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        screenWidth = dimension.width;
        screenHeight = dimension.height;

    }

    MainFrame() {
        initScreen();
        int width = 250;
        int height = 50;
        JLabel jl = new JLabel("MarkDown打包器");
        jl.setFont(new Font("楷体", Font.BOLD, 30));
        jl.setBounds(center(cWidth, width), center(cHeight, height) - 200, width, height);
        c.add(jl);

        //文本框的标签
        int jlWidth = 105;
        int jlHeight = 50;
        JLabel jLabel = new JLabel("文件路径:");
        jLabel.setBounds(center(this.cWidth, jlWidth) - 250, center(this.cHeight, jlHeight), jlWidth, jlHeight);
        jLabel.setFont(new Font("楷体", Font.BOLD, 20));
        c.add(jLabel);

        //文本框
        int jtfWidth = 450;
        int jtfHeight = 50;
        JTextField jTextField = new JTextField();
        jTextField.setBounds(center(this.cWidth, jtfWidth) + 25, center(this.cHeight, jtfHeight), jtfWidth, jtfHeight);
        jTextField.setFont(new Font("楷体", Font.BOLD, 20));
        c.add(jTextField);

        int btnWidth = 130;
        int btnHeight = 50;
        //选择文件按钮
        JButton fileChoose = new JButton("选择文件");
        fileChoose.setBounds(center(this.cWidth, btnWidth), center(this.cHeight, btnHeight) - 75, btnWidth, btnHeight);
        fileChoose.setFont(new Font("楷体", Font.BOLD, 20));
        fileChoose.addActionListener(e -> {
            System.out.println(getX());
            System.out.println(getY());
            JFileChooser jfc = new JFileChooser();
            jfc.setFileFilter(new FileFilter() {
                @Override
                public boolean accept(File f) {
                    return f.getName().endsWith("md");
                }

                @Override
                public String getDescription() {
                    return ".md";
                }
            });
            jfc.showOpenDialog(this);
            if (jfc.getSelectedFile() == null) {
                return;
            }
            String absolutePath = jfc.getSelectedFile().getAbsolutePath();
            jTextField.setText(absolutePath);

        });
        c.add(fileChoose);

        //打包按钮
        JButton jButton = new JButton("开始打包");
        jButton.setBounds(center(this.cWidth, btnWidth), center(this.cHeight, btnHeight) + 75, btnWidth, btnHeight);
        jButton.setFont(new Font("楷体", Font.BOLD, 20));
        jButton.addActionListener(e -> {
            String text = jTextField.getText();
            if (StringUtils.isBlack(StringUtils.trimAll(text))) {
                JDialog dialog = new JDialog(this);
                dialog.setTitle("warning");
                dialog.setBounds(center(this.screenWidth, 300), center(this.screenHeight, 100), 300, 100);
                JLabel content = new JLabel("请选择文件");
                dialog.add(content);
                dialog.show();
                return;
            }
            if (text.startsWith("\"")) {
                text = text.substring(1);
            }
            if (text.endsWith("\"")) {
                text = text.substring(0, text.length() - 1);
            }
            File file = new File(text);
            if (!file.exists() || !file.getName().endsWith("md")) {
                JDialog dialog = new JDialog(this);
                dialog.setTitle("warning");
                dialog.setBounds(center(this.screenWidth, 300), center(this.screenHeight, 100), 300, 100);
                JLabel content = new JLabel("文件不存在");
                if (!file.getName().endsWith("md")) {
                    content.setText("不是MarkDown文件");
                }
                dialog.add(content);
                dialog.show();
                return;
            }
            MarkDownUtils markDownUtils = new MarkDownUtils(file.getParent(), file.getName());
            markDownUtils.execute();
            jTextField.setText("");
            JDialog dialog = new JDialog(this);
            dialog.setTitle("warning");
            dialog.setBounds(center(this.screenWidth, 300), center(this.screenHeight, 100), 300, 100);
            JLabel content = new JLabel("打包完成");
            dialog.add(content);
            dialog.show();
            System.gc();
        });
        c.add(jButton);
    }

    /**
     * 初始化窗口
     */
    void initScreen() {
        c = getContentPane();
        //设置容器使用自定义布局
        c.setLayout(null);
        //设置窗口位置及大小
        setBounds(center(screenWidth, cWidth), center(screenHeight, cHeight), cWidth, cHeight);
        //设置窗口关闭规则
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        //设置窗口不可变
        setResizable(false);
    }

    /**
     * 计算居中数值（可以计算宽高）
     *
     * @param container    父容器宽\高值
     * @param subContainer 子容器宽\高值
     * @return
     */
    private int center(int container, int subContainer) {
        return (container - subContainer) / 2;
    }

    public static void main(String[] args) {
        try {
            new MainFrame().setVisible(true);
        } catch (Exception e) {

        }

    }

}
