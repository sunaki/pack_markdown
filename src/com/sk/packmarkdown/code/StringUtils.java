package com.sk.packmarkdown.code;

public class StringUtils {
    //判断空
    public static boolean isBlack(String str) {
        return str == null || str.equals("");
    }

    //去除字符串中的所有空格
    public static String trimAll(String str) {
        return str.replaceAll("\\s", "");
    }
}
