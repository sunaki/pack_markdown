package com.sk.packmarkdown.code;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    private static String DeskTop = null;

    private FileUtils() {
    }

    /**
     * 解析文件为List<String>,文件中的每一行内容 是一个list中的item
     * @param filePath 文件的绝对路径
     * @return List<String>
     */
    public static List<String> readFile(String filePath) {
        File file = new File(filePath);
        BufferedReader br = null;
        try {
            //判断是否存在
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
            br = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            List list = new ArrayList();
            String line;
            while ((line = br.readLine()) != null) {
                list.add(line);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * 输出文件
     * @param targetFilePath 输出文件的全路径(D:\??\??\??\??.??)
     * @param data data就是写入的信息 data中每一个item是写入后的一行
     */
    public static void writeFile(String targetFilePath, List<String> data) {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(targetFilePath), StandardCharsets.UTF_8));
            for (String datum : data) {
                bw.write(datum + "\n");
            }
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 文件复制
     * @param filePath 源文件路径
     * @param targetFilePath 输出路径
     */
    public static void copyFile(String filePath, String targetFilePath) {
        if (!new File(filePath).exists()) {
            return;
        }
        FileChannel inputChannel = null;
        FileChannel outputChannel = null;
        try {
            inputChannel = new FileInputStream(filePath).getChannel();
            outputChannel = new FileOutputStream(targetFilePath).getChannel();
            outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
        } catch (Exception e) {

        } finally {
            try {
                inputChannel.close();
                outputChannel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * (批量)相对路径转绝对路径
     *
     * @param basePath markdown所在的文件夹地址
     * @param paths    图片路径集合
     * @return
     */
    public static List<String> toAbsolutePaths(String basePath, List<String> paths) {
        List<String> result = new ArrayList<>();
        paths.forEach(item -> {
            result.add(toAbsolutePath(basePath, item));
        });
        return result;
    }

    /**
     * 相对路径转为绝对路径
     *
     * @param basePath markdown所在的文件夹地址
     * @param pathInfo 图片路径信息
     * @return
     */
    public static String toAbsolutePath(String basePath, String pathInfo) {
        File file = new File(pathInfo);
        //第一种情况 如果已经是绝对路径了那么一定存在所以直接返回
        if (file.exists()) {
            return file.getAbsolutePath();
        }
        File base = new File(basePath);
        //全部替换成\反斜杠.然后进行判断
        String str = pathInfo.replaceAll("/", "\\\\");
        if (str.startsWith("..\\") || str.startsWith(".\\")) {
            File file1 = new File(base + "\\" + pathInfo);
            String canonicalPath = "";
            try {
                canonicalPath = file1.getCanonicalPath();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return canonicalPath;
        } else {
            //特殊情况没有【.】或者【\】的出现
            File common = new File(basePath + "\\" + pathInfo);
            if (common.exists()) {
                return common.getAbsolutePath();
            }
//            System.err.println("出现不合法路径 -> " + pathInfo);
            return pathInfo;
        }
    }
    //获取用户电脑桌面地址(Windows)
    public static String getDeskTop() {
        if (DeskTop == null) {
            FileSystemView fileSystemView = FileSystemView.getFileSystemView();
            File homeDirectory = fileSystemView.getHomeDirectory();
            DeskTop = homeDirectory.toString();
        }
        return DeskTop;
    }

}
