package com.sk.packmarkdown.code;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MarkDownUtils {
    private String filePath;
    private String fileName;
    private String targetFilePath;
    private static final String PHOTO_FILTER_RULE = "!\\[[^\\]]+\\]\\([^\\)]+\\).*";

    /**
     *
     * @param filePath MarkDown文件所在文件夹路径
     * @param fileName MarkDown文件的名字 例如 abc.md
     */
    public MarkDownUtils(String filePath, String fileName) {
        this.filePath = filePath;
        this.fileName = fileName;
        //默认统一导出地址为用户所在的桌面下 以MarkDown文件名字作为文件夹  例如:fileName="abc.md"  那么输出地址就是 略过\桌面\abc
        this.targetFilePath = FileUtils.getDeskTop() + "\\" + fileName.substring(0, fileName.indexOf("."));
        //如果abc文件夹存在 将以abc+当前时间字符串  作为桌面唯一的文件夹
        if (new File(targetFilePath).exists()) {
            Date date = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日HH时mm分ss秒SSS毫秒");
            String format = simpleDateFormat.format(date);
            targetFilePath = FileUtils.getDeskTop() + "\\" + fileName.substring(0, fileName.indexOf(".")) + format;
        }
    }


    /**
     * 解析MarkDown文件为List<String>
     * @return List<String>
     */
    public List<String> getFileContent() {
        File file = new File(filePath + "\\" + fileName);
        if (file.exists()) {
            return FileUtils.readFile(file.getAbsolutePath());
        } else {
            try {
                throw new FileNotFoundException();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }

    }

    /**
     * 批量获取MarkDownPhoto对象
     * @param fileContent 经过FileUtils解析文件的值
     * @return List<MarkDownPhoto>
     */
    private List<MarkDownPhoto> getMarkDownPhotos(List<String> fileContent) {
        List<MarkDownPhoto> regexPhotoPath = new ArrayList<>();
        fileContent.forEach(a -> {
            //通过正则找到markdown中的图片信息
            if (StringUtils.trimAll(a).matches(PHOTO_FILTER_RULE)) {
                regexPhotoPath.add(getMarkDownPhoto(a));
            }
        });
        return regexPhotoPath;
    }

    /**
     *     获取MarkDownPhoto对象 传入通过正则过滤后的图片信息 例如:![？？？](.。。。。。。\photo\???.png)
     * @param filePhotoPath
     * @return
     */

    private MarkDownPhoto getMarkDownPhoto(String filePhotoPath) {
        MarkDownPhoto markDownPhoto = new MarkDownPhoto();
        //设置进去的值为 【![image-20230127142151213](.\photo\image-20230127142151213.png)】
        markDownPhoto.setRegexPhotoPath(filePhotoPath);
        String photoPath = filePhotoPath.substring(filePhotoPath.indexOf("(") + 1, filePhotoPath.indexOf(")"));
        //setAbsolutePhotoPath的值为【D:\???\????\???\photo\image-20230127142151213.png】
        String absolutePath = FileUtils.toAbsolutePath(this.filePath, photoPath);
        markDownPhoto.setAbsolutePhotoPath(absolutePath);
        File file = new File(absolutePath);
        //设置进去的是输出目录的图片地址
        markDownPhoto.setTargetAbsolutePhotoPath(this.targetFilePath + "\\" + "photo" + "\\" + file.getName());
        //设置进去的是输出MarkDown文件中的图片引用路径统一使用.\相对路径格式
        markDownPhoto.setTargetFilePhotoInfo(".\\photo\\" + file.getName());
        return markDownPhoto;
    }

    /**
     * 执行流程
     * 1.解析文件为List data
     * 2.通过data来匹配正则，解析成MarkDownPhoto对象
     * 3.通过List<MarkDownPhoto>的每个item.regexPhotoPath匹配data中的信息如果匹配到了就把原文中的信息替换成item.targetFilePhotoInfo信息
     * 同时再调用copy(item.absolutePhotoPath,item.targetAbsolutePhotoPath)
     * 最后在写出文件
     *
     */
    public void execute() {
        File file = new File(targetFilePath + "\\" + "photo");
        file.mkdirs();
        List<String> fileContent = getFileContent();
        List<MarkDownPhoto> markDownPhotos = getMarkDownPhotos(fileContent);
        //markDownPhotos 的索引
        int index = 0;
        for (int i = 0; i < fileContent.size(); i++) {
            MarkDownPhoto markDownPhoto = markDownPhotos.get(index);
            if (StringUtils.trimAll(fileContent.get(i)).equals(StringUtils.trimAll(markDownPhoto.getRegexPhotoPath()))) {
                //获取原文内容
                String line = fileContent.get(i);
                String newLine = line.substring(0, line.indexOf("(") + 1) + markDownPhoto.getTargetFilePhotoInfo() + line.substring(line.indexOf(")"));
                //替换原文中的值
                fileContent.set(i, newLine);
                //复制图片文件到指定导出目录
                FileUtils.copyFile(markDownPhoto.getAbsolutePhotoPath(), markDownPhoto.getTargetAbsolutePhotoPath());
                //索引加一
                index++;
                //提高效率 因为markDownPhotos的索引都到最后一位了，那么代表源文件下面几行不会存在图片文件了所以可以直接姐退出了
                if (index == markDownPhotos.size()) {
                    break;
                }
            }

        }
        //写出文件
        FileUtils.writeFile(targetFilePath + "\\" + fileName, fileContent);
    }

}
