package com.sk.packmarkdown.code;

/**
 * MarkDownPhoto用于存储MarkDown中图片信息以及转化后的信息
 */
public class MarkDownPhoto {
    //正则过滤出来值 用于替换时当做条件使用
    //格式如下![？？？？](D:\????\????\1665320692833.png)
    private String regexPhotoPath;
    //图片文件存在的绝对路径
    //格式如下D:\????\????\1665320692833.png
    private String absolutePhotoPath;
    //导出后图片所在的绝对路径 供图片导出（copy）使用
    //格式如下E:\????\photo\1665320692833.png
    private String targetAbsolutePhotoPath;
    //导出后文件中MarkDown图片引用应该填入的信息
    //格式如下.\photo\1665320692833.png
    private String targetFilePhotoInfo;

    public MarkDownPhoto() {
    }

    public MarkDownPhoto(String regexPhotoPath, String absolutePhotoPath, String targetAbsolutePhotoPath, String targetFilePhotoInfo) {
        this.regexPhotoPath = regexPhotoPath;
        this.absolutePhotoPath = absolutePhotoPath;
        this.targetAbsolutePhotoPath = targetAbsolutePhotoPath;
        this.targetFilePhotoInfo = targetFilePhotoInfo;
    }

    public String getRegexPhotoPath() {
        return regexPhotoPath;
    }

    public void setRegexPhotoPath(String regexPhotoPath) {
        this.regexPhotoPath = regexPhotoPath;
    }

    public String getAbsolutePhotoPath() {
        return absolutePhotoPath;
    }

    public void setAbsolutePhotoPath(String absolutePhotoPath) {
        this.absolutePhotoPath = absolutePhotoPath;
    }

    public String getTargetAbsolutePhotoPath() {
        return targetAbsolutePhotoPath;
    }

    public void setTargetAbsolutePhotoPath(String targetAbsolutePhotoPath) {
        this.targetAbsolutePhotoPath = targetAbsolutePhotoPath;
    }

    public String getTargetFilePhotoInfo() {
        return targetFilePhotoInfo;
    }

    public void setTargetFilePhotoInfo(String targetFilePhotoInfo) {
        this.targetFilePhotoInfo = targetFilePhotoInfo;
    }

    @Override
    public String toString() {
        return "MarkDownPhoto{" +
                "regexPhotoPath='" + regexPhotoPath + '\'' +
                ", absolutePhotoPath='" + absolutePhotoPath + '\'' +
                ", targetAbsolutePhotoPath='" + targetAbsolutePhotoPath + '\'' +
                ", targetFilePhotoInfo='" + targetFilePhotoInfo + '\'' +
                '}';
    }


}
